﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using EntityFrameworkConnection;
using Microsoft.EntityFrameworkCore;
using VacationImplementation.AssistantClasses;
using VacationsCore.Repository;
using DbUser = EntityFrameworkConnection.Core.User;

namespace VacationImplementation.Repository
{
    public class UserRepository : IRepository<User>
    {
        

        public void Delete(User entityToDelete)
        {
            Delete(entityToDelete.Id);
        }

        public void Delete(int id)
        {
            using var context = new Context();
            var removeUser = context.Users.Single(t => t.Id == id);
            context.Users.Remove(removeUser);
        }

        public IEnumerable<User> Get(Expression<Func<User, bool>> filter = null)
        {

            using var context = new Context();
            var result = context.Users.Where(user => user.Id == 5);
        }

        public User GetByID(int id)
        {
            using var context = new Context();
            var removeUser = context.Users.Single(t => t.Id == id);
            return removeUser;
        }

        public void Insert(User entity)
        {
            using var context = new Context();
            var insertUser = entity.DbUser();
            context.Users.Add(insertUser);
            context.SaveChangesAsync();
        }

        public void Update(User entityToUpdate)
        {
            using var context = new Context();
            Delete(entityToUpdate);
            Insert(entityToUpdate);
            context.SaveChangesAsync();
        }
    }
}