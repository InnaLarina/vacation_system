﻿using System;
using VacationsCore;
using DbVacationDay = EntityFrameworkConnection.Core.VacationDay;

namespace VacationImplementation
{
    public class VacationDay : IVacationDay
    {
        private VacationDay(DbVacationDay vacationDay)
        {
            Id = vacationDay.Id;
            Day = vacationDay.Day;
        }

        public int Id { get; }
        public DateTime Day { get; }

        public static implicit operator VacationDay(DbVacationDay vacationDay)
        {
            return new VacationDay(vacationDay);
        }
    }
}