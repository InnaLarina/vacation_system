﻿using System.Collections.Generic;
using VacationsCore;
using DbUser = EntityFrameworkConnection.Core.User;

namespace VacationImplementation
{
    public class User : IUser
    {
        private User(DbUser user)
        {
            Id = user.Id;
            FullName = user.FullName;
            Logins = new List<ILogin>(user.Logins.ConvertAll(t => (Login)t));
            VacationDays = new List<IVacationDay>(user.VacationDays.ConvertAll(t => (VacationDay)t));
        }

        public int Id { get; }
        public string FullName { get; }
        public List<ILogin> Logins { get; set; }
        public List<IVacationDay> VacationDays { get; set; }

        public static implicit operator User(DbUser user)
        {
            return new User(user);
        }
    }
}