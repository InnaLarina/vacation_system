﻿using VacationsCore;
using DbUser = EntityFrameworkConnection.Core.User;
using DbLogin = EntityFrameworkConnection.Core.Login;
using DbVacationDay = EntityFrameworkConnection.Core.VacationDay;

namespace VacationImplementation.AssistantClasses
{
    public static class DbExtension
    {
        public static DbUser DbUser(this IUser user)
        {
            return new DbUser(user.Id, user.FullName,
                user.Logins.ConvertAll(t => t.DbLogin()),
                user.VacationDays.ConvertAll(t => t.DbVacationDay()));
        }

        public static DbLogin DbLogin(this ILogin login)
        {
            return new DbLogin(login.Id, login.LoginString, login.PasswordHash, login.User.DbUser());
        }

        public static DbVacationDay DbVacationDay(this IVacationDay vacationDay)
        {
            return new DbVacationDay(vacationDay.Id, vacationDay.Day, vacationDay.User.DbUser());
        }
    }
}