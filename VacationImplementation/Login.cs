﻿using VacationsCore;
using DbLogin = EntityFrameworkConnection.Core.Login;

namespace VacationImplementation
{
    public class Login : ILogin
    {
        public int Id { get; }
        public string LoginString { get; }
        public string PasswordHash { get; }

        private Login(DbLogin login)
        {
            Id = login.Id;
            LoginString = login.LoginString;
            PasswordHash = login.PasswordHash;
        }
        
        public static implicit operator Login(DbLogin login)
        {
            return new Login(login);
        }
    }
}