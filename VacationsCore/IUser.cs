﻿using System.Collections.Generic;

namespace VacationsCore
{
    public interface IUser
    {
        int Id { get; }
        string FullName { get; }
        List<ILogin> Logins { get; }
        List<IVacationDay> VacationDays { get; }
    }
}