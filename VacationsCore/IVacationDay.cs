﻿using System;

namespace VacationsCore
{
    public interface IVacationDay
    {
        int Id { get; }
        DateTime Day { get; }
    }
}