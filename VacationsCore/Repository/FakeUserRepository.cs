﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace VacationsCore.Repository
{
    public class FakeUserRepository : IRepository<IUser>
    {
        //private IUser FakeUser = new User(new List<string>() { "John" }, new Vacations(new List<DateTime>() { new DateTime(20, 12, 31) }));

        void IRepository<IUser>.Delete(IUser entityToDelete)
        {
            //throw new NotImplementedException(); //TODO:
        }

        void IRepository<IUser>.Delete(int id)
        {
            //throw new NotImplementedException(); //TODO:
        }

        IEnumerable<IUser> IRepository<IUser>.Get(Expression<Func<IUser, bool>> filter)
        {
            //throw new NotImplementedException(); //TODO:
            return new List<IUser>();
        }

        IUser IRepository<IUser>.GetByID(int id)
        {
            return null;// FakeUser; //TODO:            
        }
        
        void IRepository<IUser>.Insert(IUser entity)
        {
            //throw new NotImplementedException(); //TODO:
        }

        void IRepository<IUser>.Update(IUser entityToUpdate)
        {
            //throw new NotImplementedException(); //TODO:
        }
    }
}
