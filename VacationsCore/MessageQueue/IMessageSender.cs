﻿using System;
using System.Collections.Generic;
using System.Text;


namespace VacationsCore.MessageQueue
{
    public interface IMessageSender
    {
        void SendMessage(IUser user);
    }
}
