﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VacationsCore.MessageQueue
{
    public interface IMessageRecever
    {
        List<IUser> GetUsersFromQueue();
    }
}
