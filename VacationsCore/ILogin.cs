﻿namespace VacationsCore
{
    public interface ILogin
    {
        int Id { get; }
        string LoginString { get; }
        string PasswordHash { get; }
    }
}