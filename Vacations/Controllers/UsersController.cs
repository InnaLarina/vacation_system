﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using VacationsCore;
using VacationsCore.Repository;

namespace Vacations.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly ILogger<UsersController> _logger;
        private IRepository<IUser> Repository { get; set; } = new FakeUserRepository();

        public UsersController(ILogger<UsersController> logger)
        {
            _logger = logger;
        }

        // GET: api/Users
        [HttpGet]
        public IEnumerable<IUser> Get()
        {
            _logger.LogTrace("Someone called a IEnumerable<IUser> Get()");
            return Repository.Get();
        }

        // GET: api/Users/5
        //[HttpGet("{id}", Name = "Get")]
        [HttpGet("{id}")]
        public IUser Get(Guid id)
        {
            _logger.LogTrace("Someone called a IEnumerable<IUser> Get()");
            return Repository.GetByID(id);
        }

        // POST: api/Users
        [HttpPost]
        public void Post([FromBody] IUser user)
        {
            _logger.LogTrace("Someone called a IEnumerable<IUser> Post()");
            Repository.Insert(user);
        }

        // PUT: api/Users/5
        [HttpPut("{id}")]
        public void Put([FromBody] IUser user)
        {
            _logger.LogTrace("Someone called a IEnumerable<IUser> Put()");
            Repository.Update(user);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(Guid id)
        {
            _logger.LogTrace($"Someone called a IEnumerable<IUser> Delete({id})");
            Repository.Delete(id);
        }
    }
}
