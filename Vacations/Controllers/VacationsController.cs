﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using VacationsCore;
using VacationsCore.Repository;

namespace Vacations.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VacationsController : ControllerBase
    {
        private readonly ILogger<UsersController> _logger;
        private IRepository<IUser> Repository { get; set; } = new FakeUserRepository();

        public VacationsController(ILogger<UsersController> logger)
        {
            _logger = logger;
        }

        // GET: api/Vacations
        [HttpGet]
        public IVacations Get()
        {
            _logger.LogTrace("Someone called a IEnumerable<IUser> Get()");
            return null;
        }

        // GET: api/Vacations/5
        //[HttpGet("{id}", Name = "Get")]
        [HttpGet("{id}")]
        public IVacations Get(Guid userId)
        {
            _logger.LogTrace("Someone called a IEnumerable<IUser> Get()");
            return Repository.GetByID(userId).Vacations;
        }

        // PUT: api/Vacations/5
        [HttpPut("{id}")]
        public void Put(Guid userId, [FromBody] IVacations vacations)
        {
            _logger.LogTrace("Someone called a IEnumerable<IUser> Put()");
            var user = Repository.GetByID(userId);
            user.Vacations = vacations;
            Repository.Update(user);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(Guid userId)
        {
            _logger.LogTrace("Someone called a IEnumerable<IUser> Delete()");
            var user = Repository.GetByID(userId);
            user.Vacations.Dates.Clear();
            Repository.Update(user);
        }
    }
}
