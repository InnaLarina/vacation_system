﻿using System;

namespace EntityFrameworkConnection.Core
{
    public class VacationDay
    {
        public int Id { get; set; }
        public DateTime Day { get; set; }
        public User User { get; set; }

        public VacationDay(int id, DateTime day, User user)
        {
            Id = id;
            Day = day;
            User = user;
        }
    }
}