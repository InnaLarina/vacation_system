﻿namespace EntityFrameworkConnection.Core
{
    public class Login
    {
        public int Id { get; set; }
        public string LoginString { get; set; }
        public string PasswordHash { get; set; }
        public User User { get; set; }

        public Login(int id, string loginString, string passwordHash, User user)
        {
            Id = id;
            LoginString = loginString;
            PasswordHash = passwordHash;
            User = user;
        }
    }
}