﻿using System;
using System.Collections.Generic;

namespace EntityFrameworkConnection.Core
{
    public class User
    {
        public int Id { get; set; }
        public string FullName { get; set; }

        public List<Login> Logins { get; set; }
        public List<VacationDay> VacationDays { get; set; }

        public User(int id, string fullName, List<Login> logins, List<VacationDay> vacationDays)
        {
            Id = id;
            FullName = fullName;
            Logins = logins;
            VacationDays = vacationDays;
        }
    }
}