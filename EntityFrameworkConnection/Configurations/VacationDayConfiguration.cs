﻿using EntityFrameworkConnection.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EntityFrameworkConnection.Configurations
{
    class VacationDayConfiguration : IEntityTypeConfiguration<VacationDay>
    {
        public void Configure(EntityTypeBuilder<VacationDay> builder)
        {
            builder.ToTable("VacationDays").HasKey(p => p.Id);
            builder.Property(p => p.Day).IsRequired();
            builder.HasOne(p => p.User).
                WithMany(t => t.VacationDays);
        }
    }
}
