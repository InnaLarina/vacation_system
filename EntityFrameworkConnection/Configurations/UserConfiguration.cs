﻿using EntityFrameworkConnection.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EntityFrameworkConnection.Configurations
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("Users").HasKey(p => p.Id);
            builder.Property(p => p.FullName).IsRequired();
            builder.HasMany(p => p.Logins).
                WithOne(t => t.User);
            builder.HasMany(p => p.VacationDays).
                WithOne(t => t.User);
        }
    }
}