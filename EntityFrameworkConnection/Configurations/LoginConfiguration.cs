﻿using EntityFrameworkConnection.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EntityFrameworkConnection.Configurations
{
    public class LoginConfiguration : IEntityTypeConfiguration<Login>
    {
        public void Configure(EntityTypeBuilder<Login> builder)
        {
            builder.ToTable("Logins").HasKey(p => p.Id);
            builder.Property(p => p.LoginString).IsRequired();
            builder.Property(p => p.PasswordHash).IsRequired();
            builder.HasOne(p => p.User).
                WithMany(t => t.Logins);
        }
    }
}