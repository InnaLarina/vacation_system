﻿using EntityFrameworkConnection.Configurations;
using EntityFrameworkConnection.Core;
using Microsoft.EntityFrameworkCore;

namespace EntityFrameworkConnection
{
    public class Context : DbContext
    {
        private readonly string _connectionString;
        public DbSet<User> Users { get; set; }
        public DbSet<Login> Logins { get; set; }
        public DbSet<VacationDay> VacationDays { get; set; }

        public Context(string connectionString =
            @"Data Source=79.111.0.195;Initial Catalog=OTUS_Project;User ID=server;Password=E4OyQH6cQEJzanbYb7hi; Application Name=Vacations;")
        {
            _connectionString = connectionString;
            Database.EnsureDeleted();
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UserConfiguration());
            modelBuilder.ApplyConfiguration(new LoginConfiguration());
            modelBuilder.ApplyConfiguration(new VacationDayConfiguration());
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_connectionString);
        }
    }
}